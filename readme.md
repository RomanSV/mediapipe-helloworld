# How to use
1. In the WORKSPACE file you shout set paths to your Android SDK and NDK. Note that Android SDK 31 is broken with this build for some reason, so we use Android SDK 30.
2. You should create a symlink to your Mediapipe location.
3. To open this project in Android Studio you should install the Bazel Plugin. Then go to "Import Bazel project", choose the base directory (with the WORKSPACE file) as a workspace and then select the .bazelproject file in the .aswb directory.
4. To build and install the app run `bazel mobile-install --tool_tag=ijwb:AndroidStudio -- //java/com/example/mediapipe:helloworld`
